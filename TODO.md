converter
---------

``` idris
∑ convert : C -> B
∑ bikini : B -> C
```

``` idris
(Π(x : C) convert(bikini(x)) = x) x (Π(y : B) bikini(convert(y)) = y)
```

XSyntax class ender, additional enders
--------------------------------------

``` cpp
class tmp
  ...
;
```

to

``` cpp
class tmp
  ...
```
